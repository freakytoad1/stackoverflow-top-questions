package main

import (
	"log"

	"gitlab.com/freakytoad1/stackoverflow-top-questions/stack"
)

func main() {
	// make new stack api client with pagesize of 100
	stack, err := stack.New(100)
	if err != nil {
		log.Fatalf("unable to make new stack client: %v", err)
	}

	// the desired search criteria
	// ***change these up to see different results***
	desiredTags := []string{"go"}
	desiredTitles := []string{"go"}

	// Get the ordered collection of unanswered questions
	// data desired for assignment is in 'questions'
	questions, err := stack.GetTopFiveUnanswered(desiredTags, desiredTitles)
	if err != nil {
		log.Fatalf("unable to retrieve unanswered stack questions: %v", err)
	}

	// data desired is in 'questions'
	for _, question := range questions {
		log.Printf("View Count: %d Link: %s ID: %d Date: %d", question.ViewCount, question.Link, question.QuestionID, question.CreationDate)
	}
}
