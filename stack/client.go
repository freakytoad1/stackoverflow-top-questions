package stack

import (
	"fmt"

	"gitlab.com/freakytoad1/stackoverflow-top-questions/api"
)

// Client contains http information to interact with the StackOverflow API
type Client struct {
	*api.Client
	PageSize string
	Site     string
}

// New returns a new Stack Exchange API Client with a timeout of 30s and api version of 2.2
// Pass in pageSize to set how many results are returned per page (funcs will still gather all results). A larger page size will result in faster execution time. Between 0 and 100.
func New(pageSize int) (*Client, error) {
	if pageSize > 100 || pageSize < 0 {
		return nil, fmt.Errorf("pagesize must be between 0 and 100")
	}

	return &Client{
		Client:   api.New(),
		PageSize: fmt.Sprintf("%d", pageSize),
		Site:     "stackoverflow",
	}, nil
}
