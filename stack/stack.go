package stack

import (
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strings"
	"time"

	"gitlab.com/freakytoad1/stackoverflow-top-questions/api"
)

// GetTopFiveUnanswered returns an ordered collection of JSON-serializable objects containing:
// Question ID
// Question Link
// Creation Date
// View Count
// These are ordered by viewcount and only contain the last 7 days of items.
// This returns top 5 by viewcount.
func (c *Client) GetTopFiveUnanswered(tags, titles []string) ([]*UnansweredQuestion, error) {
	// Get the current time 7 days ago
	LastWeek := time.Now().AddDate(0, 0, -7)

	items, err := c.Search(tags, titles, LastWeek)
	if err != nil {
		return nil, err
	}

	// sort by view count
	sort.Slice(items, func(p, q int) bool {
		return items[p].ViewCount > items[q].ViewCount
	})

	// change items into desired format for assignment
	// only save top 5
	questions := []*UnansweredQuestion{}
	for _, item := range items {
		newQuestion := &UnansweredQuestion{
			Link:         item.Link,
			QuestionID:   item.QuestionID,
			CreationDate: item.CreationDate,
			ViewCount:    item.ViewCount,
		}
		questions = append(questions, newQuestion)
	}

	// check the length for less than 5
	// return all if so
	if len(questions) < 5 {
		return questions, nil
	}

	// more than 5, so grab top 5
	return questions[:5], nil
}

var (
	ErrNoSearchResults = errors.New("no search results for current page")
)

// UnansweredQuestion contains information about a single unanswered question
type UnansweredQuestion struct {
	Link         string `json:"link"`          // link to question
	ViewCount    int64  `json:"view_count"`    // view count of question
	CreationDate int64  `json:"creation_date"` // date question was created, in unix epoch format
	QuestionID   int64  `json:"question_id"`   // id of the question
}

// SearchResponse is the top level response when querying the 'search' endpoint
type SearchResponse struct {
	HasMore        bool    `json:"has_more"`
	Items          []*Item `json:"items"`
	QuotaMax       int64   `json:"quota_max"`
	QuotaRemaining int64   `json:"quota_remaining"`
}

// Item is contained in SearchResponse and contains information on an Unanswered Question
type Item struct {
	AnswerCount      int64    `json:"answer_count"`
	ContentLicense   string   `json:"content_license"`
	CreationDate     int64    `json:"creation_date"`
	IsAnswered       bool     `json:"is_answered"`
	LastActivityDate int64    `json:"last_activity_date"`
	LastEditDate     int64    `json:"last_edit_date"`
	Link             string   `json:"link"`
	Owner            Owner    `json:"owner"`
	QuestionID       int64    `json:"question_id"`
	Score            int64    `json:"score"`
	Tags             []string `json:"tags"`
	Title            string   `json:"title"`
	ViewCount        int64    `json:"view_count"`
}

// Owner is contained in Item and contains information on the owner of the question
type Owner struct {
	AcceptRate   int64  `json:"accept_rate"`
	AccountID    int64  `json:"account_id"`
	DisplayName  string `json:"display_name"`
	Link         string `json:"link"`
	ProfileImage string `json:"profile_image"`
	Reputation   int64  `json:"reputation"`
	UserID       int64  `json:"user_id"`
	UserType     string `json:"user_type"`
}

// Search function.
// Inputs:   tags - an array of string terms that are tags on an unanswered question. This operates in an OR fashion. If any of the passed in tags match, it returns that question.
//          titles - an array of string terms that are keywords in an unanswered question title. This operates in an AND fashion. All terms must be in the title for it to match.
//          since - only return results created after this time
func (c *Client) Search(tags, titles []string, since time.Time) ([]*Item, error) {
	// start the recursive search starting at page 1
	return c.search(tags, titles, since, 1)
}

// internal search function also takes in a page parameter. This function recurses until the 'has_more' field returned by the API is false. All available items are returned.
func (c *Client) search(tags, titles []string, since time.Time, page int) ([]*Item, error) {
	// build the query map
	// ex) page=1&pagesize=100&order=desc&sort=creation&tagged=go;golang&intitle=gitlab;ci&site=stackoverflow
	queryMap := map[string]string{
		"order":    "desc",
		"sort":     "creation",
		"site":     c.Site,
		"fromdate": fmt.Sprintf("%d", since.Unix()), // epoch format
		"pagesize": c.PageSize,                      // max results per page
		"page":     fmt.Sprintf("%d", page),         // paging index starts at 1
	}

	// only add tags and title search if provided
	if len(tags) > 0 {
		queryMap["tagged"] = strings.Join(tags, ";")
	}
	if len(titles) > 0 {
		queryMap["intitle"] = strings.Join(titles, ";")
	}

	// create request
	req, err := c.NewRequest(http.MethodGet, "search", api.WithQuery(queryMap))
	if err != nil {
		return nil, fmt.Errorf("unable to make search request: %w", err)
	}

	// Do
	jsonResp := &SearchResponse{}
	if err := c.DoParse(req, jsonResp); err != nil {
		return nil, fmt.Errorf("unable to Do / Unmarshal request: %w", err)
	}

	// current page has no items
	if len(jsonResp.Items) < 1 {
		return nil, ErrNoSearchResults
	}

	itemsToReturn := jsonResp.Items

	// check for 'has_more'
	// Recurse until this is false
	// base case
	if jsonResp.HasMore {
		items, err := c.search(tags, titles, since, page+1)
		if errors.Is(err, ErrNoSearchResults) {
			// ignore and start to return. want to not fully error out as there were items returned at higher levels.
			// this technically shouldn't happen since 'has_more' would tell us if there are more results
			return nil, nil
		} else if err != nil {
			// any other errors we should return
			return nil, err
		}

		// append to current items
		itemsToReturn = append(itemsToReturn, items...)
	}

	// return all items
	return itemsToReturn, nil
}
