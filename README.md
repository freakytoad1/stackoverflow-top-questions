# Stackoverflow top questions

Queries the stack exchange API for the last 7 days for specified tags and title keywords, sorts the results by view count (highest to lowest), and prints the top 5 results (or less than 5 if less than 5 total). 

This code was created during a timed code assessment.

## Notes

1. `main()` is the entry point.
2. main creates a new StackExchange client and calls `GetTopFiveUnanswered` with desired search tags and titles
3. `GetTopFiveUnanswered` queries the stackexchange api for the last 7 days, sorts the results by view count (highest to lowest), and returns the top 5 results (or less than 5 if less than 5 total).
4. `main()` prints out the items.

 - Change the `tags` and `titles` in `main()` to see different results

Ways to make execution faster:
1. Don't use the `has_more'` field and batch http requests (go routines) some amount at a time, like 5 requests at a time. Check if any of those returned no results, if they all returned results, batch 5 more. Continue until one of the 5 gives no results and return all the gathered data. If there are a lot of items to be searched this could potentially speed up execution 5x (or however many are batched at one time). Do need to be careful of API rate limiting going down this route. Can get around rate-limiting by making an account and using an API key, or by using different source IPs for the code.