package api

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
)

// apiError is the format of an error from stackexchange
type apiError struct {
	ID      int    `json:"error_id"`      // HTTP code
	Message string `json:"error_message"` // Message of the error
	Name    string `json:"error_name"`    // name of the error
}

func (e *apiError) Error() string {
	return fmt.Sprintf("ID: %d Name: %s Message: %s", e.ID, e.Name, e.Message)
}

// newStackError unmarshals the stack api error into a Go error
func newStackError(respBody io.ReadCloser) error {
	// read in the body
	body, err := ioutil.ReadAll(respBody)
	if err != nil {
		return fmt.Errorf("unable to read body from stack error: %w", err)
	}

	// unmarshal response into apiError
	errResp := &apiError{}
	if err := json.Unmarshal(body, errResp); err != nil {
		return fmt.Errorf("unable to unmarshal json body from stack error: %w", err)
	}

	// return the error
	return errResp
}
