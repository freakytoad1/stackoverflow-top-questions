package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type Client struct {
	*http.Client
	Url url.URL
}

func New() *Client {
	return &Client{
		Client: &http.Client{
			Timeout: time.Second * 30, // arbitrary 30 second timeout
		},
		Url: url.URL{
			Scheme: "https",
			Host:   "api.stackexchange.com",
			Path:   "/2.2/", // base api version in instruction
		},
	}
}

func (c *Client) NewRequest(method, path string, options ...APIOption) (*http.Request, error) {
	// add additional path to base
	u := c.Url
	u.Path += path

	// base request
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("unable to make new http request: %w", err)
	}

	// perform options on request
	for _, option := range options {
		if err := option(req); err != nil {
			return nil, err
		}
	}

	return req, nil
}

// Option Pattern for adding extra info to an HTTP Request
type APIOption func(*http.Request) error

// WithQuery adds key value pairs as a raw query to the request.
// This will overwrite any existing RawQuery values !!
func WithQuery(queryMap map[string]string) APIOption {
	return func(r *http.Request) error {
		params := url.Values{}

		// add each key/value as a query parameter
		for key, value := range queryMap {
			params.Add(key, value)
		}

		// encode the values and add to url
		r.URL.RawQuery = params.Encode()
		return nil
	}
}

// DoParse performs standard Do on the request and unmarshals the reponse body into ans.
// This funtion also checks for an error response from the API.
func (c *Client) DoParse(req *http.Request, ans interface{}) error {
	// Do it
	resp, err := c.Do(req)
	if err != nil {
		return err
	}

	// Validate response
	if err := validateResponse(resp); err != nil {
		return err
	}

	// Unmarshal Body into ans
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil
	}

	if err := json.Unmarshal(body, ans); err != nil {
		return err
	}

	return nil
}

// check the response from stackexchange for known, documented errors
// https://api.stackexchange.com/docs/error-handling
func validateResponse(resp *http.Response) error {
	// all errors are 400 and above
	// assume a code less is OK
	if resp.StatusCode < 400 {
		return nil
	}

	return newStackError(resp.Body)
}
